# Voyage Extractor
Tools to extract .bf archives

Created on request from a friend several years back.
Was only tested on a couple archives for *Voyage: Inspired by Jules Verne* and may or may not work
for other games created with the same engine.
